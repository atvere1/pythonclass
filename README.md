# My project's README


Due to security reasons, writing the password in plaintext in your terminal is a bad idea, because the bash history is saved so one can just go through
the log files, the program accepts inputs only when run.

To run the program, write "python3 password.py" and follow the instructions in the program.



This script just analyses the strength of your password using length, complexity and a list of common passwords to weed out weak passwords.

Made by Andris Männik

 

