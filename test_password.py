import unittest



from password import numericCheck
from password import securityChecks

class PasswordTests (unittest.TestCase):
    def test_checks_min_length(self):
        password = securityChecks('ItExsdqwfszsdgsvwegshsgfgsgd')	
        self.assertEqual(password, True)

    def test_checks_min_length_fail(self):
        password = securityChecks('abc')
        self.assertEqual(password, False)
    def test_checks_digit_pass(self):
        password = numericCheck('12f4')
        self.assertEqual(password, True)
	
    def test_check_digit_fail(self):
        password = numericCheck('dhfa$')
        self.assertEqual(password,False)

if __name__ == "__main__":
	unittest.main()	
