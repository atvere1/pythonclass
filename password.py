#Made by Andris Männik for Fundamentals of Python course#

#Library importing#
import re
import mmap

#Asks the user for input#
#
def enterPassword():
    return input("Please enter your password: ")
    


#Checks if the password is found in a list#
def listCheck(user_input):
    return user_input not in open('passwords.txt').read()


#Checks the length of the password#

def securityChecks(user_input):
	return len(user_input) >= 10
   
    
########################################################################################
#Checks for special characters in the program#

def complexityCheck(user_input):
	return re.search(r'[&^%$#;()\\/|`~:,.<>\'"@!]',user_input)


#checks for numbers in the password#

def numericCheck(user_input):
    if (re.search(r'\d',user_input)):
        return True
    else:
        return False	


#The all green message, when you've passed all the checks#

def allGreen():
	print("Congratulations, your password is very secure, now mail the password you want to andris.mannik@gmail.com")
	print("Also try our new username checker to test it's security")

#Function that holds other functions#



def passwordRegulations():
    user_input = enterPassword()
    
    if(listCheck(user_input)):
        print("You passed the 1st security check!")
        print("|===> 25% complete")
    else:
        print("Password was found in a common dictionary, please try a more secure password!")
        passwordRegulations()
    
    if(securityChecks(user_input)):
        print("You passed the 2nd security check")
        print("|=====> 50% complete")
    else:
        print("Please enter a longer password")
        passwordRegulations()
    
    if(complexityCheck(user_input)):
        print("You passed the 3rd security check")
        print("|========> 75% complete") 
    else:
        print ("Your password needs to have a special character in it")
        passwordRegulations()

    if(numericCheck(user_input)):
        print("You passed the 4th security check")
        print("|=========> 100% complete")
        allGreen()
    else:
        print ("You must have at least one number in your password")
        passwordRegulations()

#Start of program#
if __name__ == "__main__":
	passwordRegulations()	



#passwordLengthTest()

#assert listCheck("Passed") == True, "Passed should not be in password.txt file"
#assert listCheck("test") == False, "Test is in the password.txt file"
#assert complexityCheck("Howdy4^") == True, "The test would pass because there's a special character in it."


